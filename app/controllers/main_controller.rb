class MainController < ApplicationController
	include ActionView::Helpers::UrlHelper

	def foo(controller_binding)
  		html =  Stack.find(3).text1
  		template = ERB.new(html)
  		template.result(controller_binding)
	end

	def my

    	@Stack = Stack.all
        html =  Stack.find(5)
        render :inline  => html.text1
	end

	def get_binding # this is only a helper method to access the objects binding method
		binding
  	end

end