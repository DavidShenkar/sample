class CreateStacks < ActiveRecord::Migration
  def change
    create_table :stacks do |t|
      t.string :text1
      t.string :text2
      t.string :text3
      t.string :text4

      t.timestamps
    end
  end
end
